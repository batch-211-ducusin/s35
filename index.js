const express = require('express');

// si Mongoose is a package that allows creation of schemas to model our data structures.
// Also has access to a number of method for manipulating our database
const mongoose = require('mongoose');
const app = express();
const port = 3001;


//MongoDB connection
/*
	- Connect to the database by passing in our connection string (kinopy kay mongoDB), remember to replace the password and database names with actual values.

	
	Syntax:
	mongoose.connect("<MongoDB Connection String>", {useNewUrlParser: true})


	- Si {newUrlParser: true} Allows us to aviod any current and future errors while connecting to MongoDB
*/
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.cxdyx16.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// we notifications for connection success or failure.
// itong let ang ang hohold ng connection natin.
// Allows us to handle errors when the initial connection is established.
//works with the on and once Mongoose methods.
let db = mongoose.connection;


// if a connection error occured, ourput in the console.
// sa madaling salita, kapag may error tayo, ito ang mag pprint.
// console.error.bind(console) Allows us to print errors in our terminal.
db.on("error", console.error.bind(console,"connectino error"));


db.once("open", () => console.log("We're connected to the cloud database"))


// Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database.
// Schemas act as blueprints to our data.
// Use the Schema() constructor of the Mongoose module to create a new Schema.
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({

	// Define the fields with their corresponding data type
	name: String,
	status: {
		type: String,
		default: "pending"
	}

});



// Models
// Uses Schemas and are used to create or instantiate objects that correspond to the schema.
// Server>Schema(blueprint)>Database>Collection
const Task = mongoose.model("Task", taskSchema);


// Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended: true}));




// Create a new task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error.
		- If the task does not exist in the database, we add it in the database.
	2. The task data will be coming from the request's body.
	3. Create a new Task object with a "name" field/property.
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object.
*/
app.post("/tasks", (req, res) =>{

	Task.findOne({name:req.body.name},(err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created");
				}
			})
		}
	})

});

// Getting all the tasks
// Business Logic
/*
	1. retrieve all the documents
	2. if an error is encountered, print the error
	3. if no errors are found, send a success status back to the client or Postman and return an array of documents.
*/
app.get("/tasks", (req, res) => {
	Task.find({},(err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})





// Activity s35
// 1. User schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// 2. User model
const User = mongoose.model("User", userSchema);

// Post route and request
app.post("/signup", (req, res) =>{

	User.findOne({ username: req.body.username },(err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate user found");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(200).send("New user registered");
				}
			})
		}
	})

});

//Stretch Goal
//retrieve all users
app.get("/all", (req, res) => {
	User.find({},(err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})




app.listen(port,() => console.log(`Server running at port ${port}`));


//Activity Solution
// Registering a user
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- if the user already exist in the databasem we return an error
	- if the user doesn't exist in the database, we add it in the database.
2. The user data will be coming from the request's body.
3. Create a new User object with a "username" and "password" fields or properties.
*/

/*const userSchema = new mongoose.Schema({
	username: String,
	password: String
});
const User = mongoose.model("User", userSchema);

//register a user

app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username}, (error, result) => {
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate username found");
		} else {
			if(request.body.username !== " " && request.body.password !== " "){
				let newUser = new User ({
					username: request.body.username,
					password: request.body.password
				});

				newUser.save((saveErr, savedTask) => {
					if(saveErr){
						return console.log(saveErr);
					} else {
						return response.status(201).send("New user registered")
					}
				})
			} else {
				return response.send("Both username and password must be provided");
			}
		}
	})
})
*/