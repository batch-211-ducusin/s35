//Express.JS - Data Persistence via Mongoose ODM
/*
- Translate JS Object to manage a database.
*/
/*
What is an ODM?
	- An Object Document Mapper is a tool that translates objects in code to documents for use in document-based databases such as MongoDB.

What is Mongoose?
	- An ODM library that mangaes data relationships, validates chemas, and simplifies MongoDB document manipulation via the use of models.

Schemas
	- A schema is a representation of a document's structure. it also contains a codument's expected properties and data types.

Models
	- A programming interface for querying or manipulating a database. A mongoose model contains methods that simplify such operations.
*/